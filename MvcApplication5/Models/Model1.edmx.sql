
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 09/26/2017 13:48:26
-- Generated from EDMX file: c:\users\user\documents\visual studio 2012\Projects\MvcApplication5\MvcApplication5\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [AdventureWorks2014];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[HumanResources].[FK_EmployeeDepartmentHistory_Department_DepartmentID]', 'F') IS NOT NULL
    ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] DROP CONSTRAINT [FK_EmployeeDepartmentHistory_Department_DepartmentID];
GO
IF OBJECT_ID(N'[HumanResources].[FK_EmployeeDepartmentHistory_Employee_BusinessEntityID]', 'F') IS NOT NULL
    ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] DROP CONSTRAINT [FK_EmployeeDepartmentHistory_Employee_BusinessEntityID];
GO
IF OBJECT_ID(N'[HumanResources].[FK_EmployeeDepartmentHistory_Shift_ShiftID]', 'F') IS NOT NULL
    ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] DROP CONSTRAINT [FK_EmployeeDepartmentHistory_Shift_ShiftID];
GO
IF OBJECT_ID(N'[HumanResources].[FK_EmployeePayHistory_Employee_BusinessEntityID]', 'F') IS NOT NULL
    ALTER TABLE [HumanResources].[EmployeePayHistory] DROP CONSTRAINT [FK_EmployeePayHistory_Employee_BusinessEntityID];
GO
IF OBJECT_ID(N'[HumanResources].[FK_JobCandidate_Employee_BusinessEntityID]', 'F') IS NOT NULL
    ALTER TABLE [HumanResources].[JobCandidate] DROP CONSTRAINT [FK_JobCandidate_Employee_BusinessEntityID];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[HumanResources].[Department]', 'U') IS NOT NULL
    DROP TABLE [HumanResources].[Department];
GO
IF OBJECT_ID(N'[HumanResources].[Employee]', 'U') IS NOT NULL
    DROP TABLE [HumanResources].[Employee];
GO
IF OBJECT_ID(N'[HumanResources].[EmployeeDepartmentHistory]', 'U') IS NOT NULL
    DROP TABLE [HumanResources].[EmployeeDepartmentHistory];
GO
IF OBJECT_ID(N'[HumanResources].[EmployeePayHistory]', 'U') IS NOT NULL
    DROP TABLE [HumanResources].[EmployeePayHistory];
GO
IF OBJECT_ID(N'[HumanResources].[JobCandidate]', 'U') IS NOT NULL
    DROP TABLE [HumanResources].[JobCandidate];
GO
IF OBJECT_ID(N'[HumanResources].[Shift]', 'U') IS NOT NULL
    DROP TABLE [HumanResources].[Shift];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Departments'
CREATE TABLE [dbo].[Departments] (
    [DepartmentID] smallint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [GroupName] nvarchar(50)  NOT NULL,
    [ModifiedDate] datetime  NOT NULL
);
GO

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [BusinessEntityID] int  NOT NULL,
    [NationalIDNumber] nvarchar(15)  NOT NULL,
    [LoginID] nvarchar(256)  NOT NULL,
    [OrganizationLevel] smallint  NULL,
    [JobTitle] nvarchar(50)  NOT NULL,
    [BirthDate] datetime  NOT NULL,
    [MaritalStatus] nchar(1)  NOT NULL,
    [Gender] nchar(1)  NOT NULL,
    [HireDate] datetime  NOT NULL,
    [SalariedFlag] bit  NOT NULL,
    [VacationHours] smallint  NOT NULL,
    [SickLeaveHours] smallint  NOT NULL,
    [CurrentFlag] bit  NOT NULL,
    [rowguid] uniqueidentifier  NOT NULL,
    [ModifiedDate] datetime  NOT NULL
);
GO

-- Creating table 'EmployeeDepartmentHistories'
CREATE TABLE [dbo].[EmployeeDepartmentHistories] (
    [BusinessEntityID] int  NOT NULL,
    [DepartmentID] smallint  NOT NULL,
    [ShiftID] tinyint  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NULL,
    [ModifiedDate] datetime  NOT NULL
);
GO

-- Creating table 'EmployeePayHistories'
CREATE TABLE [dbo].[EmployeePayHistories] (
    [BusinessEntityID] int  NOT NULL,
    [RateChangeDate] datetime  NOT NULL,
    [Rate] decimal(19,4)  NOT NULL,
    [PayFrequency] tinyint  NOT NULL,
    [ModifiedDate] datetime  NOT NULL
);
GO

-- Creating table 'JobCandidates'
CREATE TABLE [dbo].[JobCandidates] (
    [JobCandidateID] int IDENTITY(1,1) NOT NULL,
    [BusinessEntityID] int  NULL,
    [Resume] nvarchar(max)  NULL,
    [ModifiedDate] datetime  NOT NULL
);
GO

-- Creating table 'Shifts'
CREATE TABLE [dbo].[Shifts] (
    [ShiftID] tinyint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [StartTime] time  NOT NULL,
    [EndTime] time  NOT NULL,
    [ModifiedDate] datetime  NOT NULL
);
GO

-- Creating table 'Photos'
CREATE TABLE [dbo].[Photos] (
    [PhotoID] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [PhotoFile] varbinary(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [Owner] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Comments'
CREATE TABLE [dbo].[Comments] (
    [CommentID] int IDENTITY(1,1) NOT NULL,
    [User] nvarchar(max)  NOT NULL,
    [Subject] nvarchar(max)  NOT NULL,
    [Body] nvarchar(max)  NOT NULL,
    [PhotoPhotoID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [DepartmentID] in table 'Departments'
ALTER TABLE [dbo].[Departments]
ADD CONSTRAINT [PK_Departments]
    PRIMARY KEY CLUSTERED ([DepartmentID] ASC);
GO

-- Creating primary key on [BusinessEntityID] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC);
GO

-- Creating primary key on [BusinessEntityID], [DepartmentID], [ShiftID], [StartDate] in table 'EmployeeDepartmentHistories'
ALTER TABLE [dbo].[EmployeeDepartmentHistories]
ADD CONSTRAINT [PK_EmployeeDepartmentHistories]
    PRIMARY KEY CLUSTERED ([BusinessEntityID], [DepartmentID], [ShiftID], [StartDate] ASC);
GO

-- Creating primary key on [BusinessEntityID], [RateChangeDate] in table 'EmployeePayHistories'
ALTER TABLE [dbo].[EmployeePayHistories]
ADD CONSTRAINT [PK_EmployeePayHistories]
    PRIMARY KEY CLUSTERED ([BusinessEntityID], [RateChangeDate] ASC);
GO

-- Creating primary key on [JobCandidateID] in table 'JobCandidates'
ALTER TABLE [dbo].[JobCandidates]
ADD CONSTRAINT [PK_JobCandidates]
    PRIMARY KEY CLUSTERED ([JobCandidateID] ASC);
GO

-- Creating primary key on [ShiftID] in table 'Shifts'
ALTER TABLE [dbo].[Shifts]
ADD CONSTRAINT [PK_Shifts]
    PRIMARY KEY CLUSTERED ([ShiftID] ASC);
GO

-- Creating primary key on [PhotoID] in table 'Photos'
ALTER TABLE [dbo].[Photos]
ADD CONSTRAINT [PK_Photos]
    PRIMARY KEY CLUSTERED ([PhotoID] ASC);
GO

-- Creating primary key on [CommentID] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [PK_Comments]
    PRIMARY KEY CLUSTERED ([CommentID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [DepartmentID] in table 'EmployeeDepartmentHistories'
ALTER TABLE [dbo].[EmployeeDepartmentHistories]
ADD CONSTRAINT [FK_EmployeeDepartmentHistory_Department_DepartmentID]
    FOREIGN KEY ([DepartmentID])
    REFERENCES [dbo].[Departments]
        ([DepartmentID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeDepartmentHistory_Department_DepartmentID'
CREATE INDEX [IX_FK_EmployeeDepartmentHistory_Department_DepartmentID]
ON [dbo].[EmployeeDepartmentHistories]
    ([DepartmentID]);
GO

-- Creating foreign key on [BusinessEntityID] in table 'EmployeeDepartmentHistories'
ALTER TABLE [dbo].[EmployeeDepartmentHistories]
ADD CONSTRAINT [FK_EmployeeDepartmentHistory_Employee_BusinessEntityID]
    FOREIGN KEY ([BusinessEntityID])
    REFERENCES [dbo].[Employees]
        ([BusinessEntityID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [BusinessEntityID] in table 'EmployeePayHistories'
ALTER TABLE [dbo].[EmployeePayHistories]
ADD CONSTRAINT [FK_EmployeePayHistory_Employee_BusinessEntityID]
    FOREIGN KEY ([BusinessEntityID])
    REFERENCES [dbo].[Employees]
        ([BusinessEntityID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [BusinessEntityID] in table 'JobCandidates'
ALTER TABLE [dbo].[JobCandidates]
ADD CONSTRAINT [FK_JobCandidate_Employee_BusinessEntityID]
    FOREIGN KEY ([BusinessEntityID])
    REFERENCES [dbo].[Employees]
        ([BusinessEntityID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCandidate_Employee_BusinessEntityID'
CREATE INDEX [IX_FK_JobCandidate_Employee_BusinessEntityID]
ON [dbo].[JobCandidates]
    ([BusinessEntityID]);
GO

-- Creating foreign key on [ShiftID] in table 'EmployeeDepartmentHistories'
ALTER TABLE [dbo].[EmployeeDepartmentHistories]
ADD CONSTRAINT [FK_EmployeeDepartmentHistory_Shift_ShiftID]
    FOREIGN KEY ([ShiftID])
    REFERENCES [dbo].[Shifts]
        ([ShiftID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeDepartmentHistory_Shift_ShiftID'
CREATE INDEX [IX_FK_EmployeeDepartmentHistory_Shift_ShiftID]
ON [dbo].[EmployeeDepartmentHistories]
    ([ShiftID]);
GO

-- Creating foreign key on [PhotoPhotoID] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_PhotoComment]
    FOREIGN KEY ([PhotoPhotoID])
    REFERENCES [dbo].[Photos]
        ([PhotoID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PhotoComment'
CREATE INDEX [IX_FK_PhotoComment]
ON [dbo].[Comments]
    ([PhotoPhotoID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
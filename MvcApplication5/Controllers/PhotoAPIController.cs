﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MvcApplication5.Models;

namespace MvcApplication5.Controllers
{
    public class PhotoAPIController : ApiController
    {
        private AdventureWorks2014Entities1 db = new AdventureWorks2014Entities1();

        // GET api/PhotoAPI
        public IEnumerable<Photo> GetPhotos()
        {
            List<Photo> result = new List<Photo>();
            Photo temp;
            foreach (var emp in db.Photos.ToList())
            {
                temp = new Photo()
                {
                    Title = emp.Title,
                    PhotoFile = emp.PhotoFile,
                    Description = emp.Description,
                    CreatedDate = emp.CreatedDate,
                    Owner = emp.Owner
                };
                result.Add(temp);
            }

            return result;
            
            //return db.Photos.AsEnumerable();
        }

        // GET api/PhotoAPI/5
        public Photo GetPhoto(int id)
        {
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return new Photo()
            {
                PhotoID = photo.PhotoID,
                Title = photo.Title
            };
        }

        // PUT api/PhotoAPI/5
        public HttpResponseMessage PutPhoto(int id, Photo photo)
        {
            if (ModelState.IsValid && id == photo.PhotoID)
            {
                db.Entry(photo).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/PhotoAPI
        public HttpResponseMessage PostPhoto(Photo photo)
        {
            if (ModelState.IsValid)
            {
                db.Photos.Add(photo);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, photo);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = photo.PhotoID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/PhotoAPI/5
        public HttpResponseMessage DeletePhoto(int id)
        {
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Photos.Remove(photo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, photo);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using MvcApplication5.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication5.Controllers
{
    public class PhotoController : Controller
    {
        AdventureWorks2014Entities1 advEntity = new AdventureWorks2014Entities1();

        private static Logger log = LogManager.GetCurrentClassLogger();
        
        //
        // GET: /Photo/

        public ActionResult Index()
        {
            return View(advEntity.Photos.ToList());
        }

        public ActionResult Prefix(String id = "")
        {
            //var hasil = from x in advEntity.Photos
            //            where x.Title.StartsWith(id) | x.Comments.Count > 1
            //            select x;
            var hasil = from x in advEntity.Photos
                        where x.Title.Contains(id) | x.Description.Contains(id) | x.Owner.Contains(id)
                        select x;
            log.Debug(hasil.Count());
            return View("Index", hasil.ToList());
        }

        [HttpGet]
        public ActionResult tambah()
        {
            return View();
        }

        [HttpPost]
        public ActionResult tambah(Photo photo){
            if (ModelState.IsValid)
            {

                //var advEntity = new AdventureWorks2014Entities1();
                //var photo = new Photo();

                //photo.Title = "ini Judul Photo";
                photo.PhotoFile = new byte[64];
                //photo.Description = "ini deskripsi";
                //photo.CreatedDate = DateTime.Now;
                //photo.Owner = "owner";

                var komen = new Comment();
                komen.PhotoPhotoID = 2;
                komen.User = "Makmur";
                komen.Subject = "Warna";
                komen.Body = "Kurang cemerlang warnanya";

                advEntity.Photos.Add(photo);
                advEntity.Comments.Add(komen);
                advEntity.SaveChanges();

                ViewBag.photo = photo;
                return View();
            }
            return View(photo);
        }

        //
        // GET: /Photo/Edit/5
        [HttpGet]
        public ActionResult edit(short id = 0)
        {
            Photo editphoto = advEntity.Photos.Find(id);
            if (editphoto == null)
            {
                return HttpNotFound();
            }
            return View(editphoto);
        }

        //
        // POST: /Photo/Edit/5

        [HttpPost]
        public ActionResult edit(Photo photo)
        {
            if (ModelState.IsValid)
            {
                advEntity.Entry(photo).State = EntityState.Modified;
                advEntity.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(photo);
        }

        //
        // GET: /Photo/Delete/5
        [HttpGet]
        public ActionResult Delete(short id = 0)
        {
            Photo hapusphoto = advEntity.Photos.Find(id);
            if (hapusphoto == null)
            {
                List<int> idComment = new List<int>();

                foreach (var comment in hapusphoto.Comments)
                {
                    idComment.Add(comment.CommentID);
                }

                foreach (var idC in idComment)
                {
                    advEntity.Comments.Remove(advEntity.Comments.Find(idC));
                }
                advEntity.Photos.Remove(hapusphoto);
                advEntity.SaveChanges();
            }
            return View(hapusphoto);
        }

        //
        // POST: /Photo/Delete/5
        //nyoba ada gitnya gak
        //nyoba bitbucket coy
        //keempat
        //keempat - tapi barus kelima
        //keenam - tapi baris keenam

        [HttpPost]      
        public ActionResult Delete(Photo photo)
        {
            if (ModelState.IsValid)
            {
                advEntity.Entry(photo).State = EntityState.Modified;
                advEntity.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(photo);
        }
    }
}

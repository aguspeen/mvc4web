﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcApplication5.Models.Comment>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>Comment</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.User) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.User) %>
            <%: Html.ValidationMessageFor(model => model.User) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Subject) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Subject) %>
            <%: Html.ValidationMessageFor(model => model.Subject) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Body) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Body) %>
            <%: Html.ValidationMessageFor(model => model.Body) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PhotoPhotoID, "Photo") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("PhotoPhotoID", String.Empty) %>
            <input type =" text" id="namaPhoto" />
            <%: Html.ValidationMessageFor(model => model.PhotoPhotoID) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/jqueryval") %>

     <script>
         $(document).ready(function () {
             $.getJSON("../api/photoAPI", function (data) {
                 var photo = [];

                 $.each(data, function (idx, row) {
                     photo[idx] =
                         {
                             'label': row.Title,
                             'value' : row.PhotoID
                         };
                 });


                 $("#namaPhoto").autocomplete({
                     minLength: 1,
                     source: photo,
                     select: function (e, ui) {
                         e.preventDefault();
                         $(this).val(ui.item.label);
                         $("#PID").val(ui.item.value);
                     }
                 });
             });
         });
    </script>
</asp:Content>

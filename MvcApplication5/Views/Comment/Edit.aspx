﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcApplication5.Models.Comment>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Edit</h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>Comment</legend>

        <%: Html.HiddenFor(model => model.CommentID) %>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.User) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.User) %>
            <%: Html.ValidationMessageFor(model => model.User) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Subject) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Subject) %>
            <%: Html.ValidationMessageFor(model => model.Subject) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Body) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Body) %>
            <%: Html.ValidationMessageFor(model => model.Body) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PhotoPhotoID, "Photo") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("PhotoPhotoID", String.Empty) %>
            <%: Html.ValidationMessageFor(model => model.PhotoPhotoID) %>
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/jqueryval") %>
</asp:Content>

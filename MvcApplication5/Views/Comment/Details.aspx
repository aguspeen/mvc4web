﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcApplication5.Models.Comment>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Details</h2>

<fieldset>
    <legend>Comment</legend>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.User) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.User) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Subject) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Subject) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Body) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Body) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Photo.Title) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Photo.Title) %>
    </div>
</fieldset>
<p>

    <%: Html.ActionLink("Edit", "Edit", new { id=Model.CommentID }) %> |
    <%: Html.ActionLink("Back to List", "Index") %>
</p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>

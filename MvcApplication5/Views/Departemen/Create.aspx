﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcApplication5.Models.Department>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>Department</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Name) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Name) %>
            <%: Html.ValidationMessageFor(model => model.Name) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.GroupName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.GroupName) %>
            <%: Html.ValidationMessageFor(model => model.GroupName) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ModifiedDate) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ModifiedDate) %>
            <%: Html.ValidationMessageFor(model => model.ModifiedDate) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.HoD) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.HoD) %>
            <input type =" text" id="namaHoD" />
            <%: Html.ValidationMessageFor(model => model.HoD) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/jqueryval") %>

    <script>
        $(document).ready(function () {
            //$("#namaHoD").change(function () {
            //    var panjang = ($(this).val().length) * 65;
            //    $(this).animate({
            //        width: panjang + "px",
            //        backgroundColor : "gold"
            //    },5000);
            //});

            $.getJSON("../api/employeeAPI", function (data) {
                var employee = [];

                $.each(data, function (idx, row) {
                    employee[idx] =
                        {
                            'label' : row.LoginID,
                            'value' : row.BusinessEntityID
                        };
                });


            $("#namaHoD").autocomplete({
                    minLength : 2,
                    source: employee,
                    select: function (e, ui) {
                        e.preventDefault();
                        $(this).val(ui.item.label);
                        $("#HoD").val(ui.item.value);
                    }
                });
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $("#ModifiedDate").datepicker({
                dateFormat: "dd MM yy",
                regional: "id"
            });

        });
    </script>
</asp:Content>

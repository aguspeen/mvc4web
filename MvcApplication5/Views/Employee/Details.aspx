﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcApplication5.Models.Employee>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Details</h2>

<fieldset>
    <legend>Employee</legend>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.BusinessEntityID) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.BusinessEntityID) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.NationalIDNumber) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NationalIDNumber) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.LoginID) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.LoginID) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.OrganizationLevel) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.OrganizationLevel) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.JobTitle) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.JobTitle) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.BirthDate) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.BirthDate) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.MaritalStatus) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.MaritalStatus) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.Gender) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Gender) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.HireDate) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.HireDate) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.SalariedFlag) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.SalariedFlag) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.VacationHours) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.VacationHours) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.SickLeaveHours) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.SickLeaveHours) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.CurrentFlag) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.CurrentFlag) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.rowguid) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.rowguid) %>
    </div>

    <div class="display-label">
        <%: Html.DisplayNameFor(model => model.ModifiedDate) %>
    </div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.ModifiedDate) %>
    </div>
</fieldset>
<p>
    <%: Html.ActionLink("Edit", "Edit", new { /* id=Model.PrimaryKey */ }) %> |
    <%: Html.ActionLink("Back to List", "Index") %>
</p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
